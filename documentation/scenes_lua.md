# Scenes

On doit dériver (en public) de `ge::core::scenes::Scene` pour créer une nouvelle scène.

Les fonctions à modifier sont:

* handleEvent
* update
* render

Niveau scripting, il y a besoin de ces fonctions:

* onStateChange(state: str), state étant soit `running`, `idle` ou `stop`
* onEvent(event: table)
* onUpdate(dt: float)
* onRender(window: table)

La table pour l'event est de ce format là:

La table pour la window est de ce format là: