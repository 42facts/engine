#ifndef ge_config_loader_hpp
#define ge_config_loader_hpp

#include <string>
#include <json/json.h>
#include <ge/files/exists.hpp>

namespace ge
{
    namespace config
    {
        class Loader
        {
        public:
            Loader(const std::string& file);
            ~Loader();

            void reload();

            inline Json::Value& operator()()
            {
                return m_content;
            }
        
        private:
            Json::Value m_content;
            std::string m_filename;
        };
    }
}

#endif  // ge_config_loader_hpp