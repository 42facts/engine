#ifndef ge_network_httpclient_hpp
#define ge_network_httpclient_hpp

#include <httplib.hpp>
#include <functional>
#include <string>
#include <memory>
#include <mutex>

namespace ge
{
    namespace network
    {
        class HTTPClient
        {
        public:
            using Callback_t = std::function<void(std::shared_ptr<httplib::Response> res)>;

            HTTPClient(const char* host, int port);
            ~HTTPClient();

            void setMainRoute(const std::string& mainRoute);
            
            void post(const std::string& route, const std::string& msg, HTTPClient::Callback_t callback);
            void get(const std::string& route, HTTPClient::Callback_t callback);
            
            void waitPost(const std::string& route, const std::string& msg, HTTPClient::Callback_t callback);
            void waitGet(const std::string& route, HTTPClient::Callback_t callback);

            void setHeader(const std::string& name, const std::string& value);

        private:
            std::string m_mainRoute;
            httplib::Client m_client;
            std::mutex m_mutex;
            httplib::Headers m_headers;
        };
    }
}

#endif  // ge_network_httpclient_hpp