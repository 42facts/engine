#ifndef ge_core_engine_hpp
#define ge_core_engine_hpp

#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <string>
#include <functional>

#include <ge/constants.hpp>
#include <ge/core/scenes/manager.hpp>

#include <imgui/imgui.h>
#include <imgui/imgui-SFML.h>

namespace ge
{
    namespace core
    {
        struct ImGUI
        {
            sf::Color bgColor;
            float color[3] = { 0.f, 0.f, 0.f };
            char windowTitle[255] = "ImGui + SFML = <3";
        };

        class Engine
        {
        public:
            using EventFunc = std::function<void(const sf::Event& ev)>;
            using UpdateFunc = std::function<void(sf::Time dt)>;
            using RenderFunc = std::function<void(sf::RenderTarget& target)>;

            Engine(unsigned width, unsigned height);
            ~Engine();

            void run();
            void handleEvent();
            void update(sf::Time dt);
            void render();

            void setTitle(const std::string& name);
            void setVSync(bool value);
            void setImGUIDebug(bool value);
            void setFPSLimit(int value);

            void setEventFunc(EventFunc event = [](const sf::Event&){});
            void setUpdateFunc(UpdateFunc update = [](sf::Time){});
            void setRenderFunc(RenderFunc render = [](sf::RenderTarget&){});
        
            template<typename S, typename... Args>
            std::size_t addScene(Args&&... args)
            {
                return m_sceneManager.addScene<S>(std::forward<Args>(args)...);
            }
            void removeScene(std::size_t id);

            void setCurrentScene(std::size_t id);

        private:
            sf::RenderWindow m_window;
            sf::Clock m_clock;
            EventFunc m_eventFunc;
            UpdateFunc m_updateFunc;
            RenderFunc m_renderFunc;

            bool m_imgui_debug;
            ImGUI m_imgui;

            ge::core::scenes::Manager m_sceneManager;
        };
    }
}

#endif  // ge_core_engine_hpp