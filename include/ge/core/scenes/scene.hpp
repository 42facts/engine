#ifndef ge_core_scenes_scene_hpp
#define ge_core_scenes_scene_hpp

#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <string>

#include <ge/scripting/LuaScripting.hpp>
#include <ge/scripting/ArkScripting.hpp>

namespace ge
{
    namespace core
    {
        namespace scenes
        {
            enum class State
            {
                Running,
                Idle,  // put in the background
                Stop
            };

            class Scene
            {
            public:
                Scene(std::size_t id);
                virtual ~Scene() {}

                virtual void handleEvent(const sf::Event& event) = 0;
                virtual void update(sf::Time dt) = 0;
                virtual void render(sf::RenderTarget& window) = 0;

                friend class Manager;
            
            protected:
                std::size_t getId();
                State getState();
                virtual void setState(State state);

                virtual void _init();
                virtual void _handleEvent(const sf::Event& event);
                virtual void _update(sf::Time dt);
                virtual void _render(sf::RenderTarget& window);

                State m_state;
            
            private:
                std::size_t m_id;
            };

            class LuaScene : public Scene
            {
            public:
                LuaScene(std::size_t id, const std::string& script);
                ~LuaScene();

                virtual void handleEvent(const sf::Event& event) = 0;
                virtual void update(sf::Time dt) = 0;
                virtual void render(sf::RenderTarget& window) = 0;

                friend class Manager;
            
            protected:
                void setState(State state);

                void _init();
                void _handleEvent(const sf::Event& event);
                void _update(sf::Time dt);
                void _render(sf::RenderTarget& window);

                ge::scripting::LuaScripting m_lua;
            };

            class ArkScene : public Scene
            {
            public:
                ArkScene(std::size_t id, const std::string& script, const std::string& libdir="arklib/");
                ~ArkScene();

                virtual void handleEvent(const sf::Event& event) = 0;
                virtual void update(sf::Time dt) = 0;
                virtual void render(sf::RenderTarget& window) = 0;

                friend class Manager;
            
            protected:
                void setState(State state);

                void _init();
                void _handleEvent(const sf::Event& event);
                void _update(sf::Time dt);
                void _render(sf::RenderTarget& window);

                ge::scripting::ArkScripting m_ark;
            };
        }
    }
}

#endif  // ge_core_scenes_scene_hpp