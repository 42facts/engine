#ifndef ge_core_scenes_manager_hpp
#define ge_core_scenes_manager_hpp

#include <vector>
#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include <ge/core/scenes/scene.hpp>

namespace ge
{
    namespace core
    {
        namespace scenes
        {
            class Manager
            {
            public:
                Manager();
                ~Manager();

                template<typename S, typename... Args>
                std::size_t addScene(Args&&... args)
                {
                    m_scenes.push_back(std::make_unique<S>(m_scenes.size(), std::forward<Args>(args)...));
                    m_scenes.back()->_init();
                    return m_scenes.size() - 1;
                }
                void removeScene(std::size_t id);

                Scene* getCurrentScene();
                void setCurrentScene(std::size_t id);

                void handleEvent(const sf::Event& event);
                void update(sf::Time dt);
                void render(sf::RenderTarget& window);
            
            private:
                std::vector<std::unique_ptr<Scene>> m_scenes;
                std::size_t m_current;
                bool m_active;
            };
        }
    }
}

#endif  // ge_core_scenes_manager_hpp