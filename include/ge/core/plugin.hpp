#ifndef ge_core_plugin_hpp
#define ge_core_plugin_hpp

#include <ge/constants.hpp>

#if defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
    #include <Windows.h>
#elif defined(GE_SYS_UNIX) || defined(GE_SYS_APPLE)
    #include <dlfcn.h>
#else
    #error "Can not identify the platform on which you are running, aborting"
#endif

#include <string>
#include <system_error>
#include <exception>

namespace ge
{
    namespace core
    {
        class SharedLibrary
        {
        public:
            SharedLibrary();
            SharedLibrary(const std::string& path);
            ~SharedLibrary();

            void load(const std::string& path);
            void unload();

            template <typename T>
            T get(const std::string& procname)
            {
                T funcptr;

#if defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
                if (NULL == (funcptr = reinterpret_cast<T>(GetProcAddress(m_hInstance, procname.c_str()))))
                {
                    throw std::system_error(
                        std::error_code(::GetLastError(), std::system_category())
                        , std::string("Couldn't find ") + procname
                    );
                }
#elif defined(GE_SYS_UNIX) || defined(GE_SYS_APPLE)
                if (NULL == (funcptr = reinterpret_cast<T>(dlsym(m_hInstance, procname.c_str()))))
                {
                    throw std::system_error(
                        std::error_code(errno, std::system_category())
                        , std::string("Couldn't find ") + procname + ", " + std::string(dlerror())
                    );
                }
#endif
                return funcptr;
            }
        
        private:
#if defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
            HINSTANCE m_hInstance;
#elif defined(GE_SYS_UNIX) || defined(GE_SYS_APPLE)
            void* m_hInstance;
#endif
            std::string m_path;
            bool m_loaded;
        };
    }
}

#endif  // ge_core_plugin_hpp