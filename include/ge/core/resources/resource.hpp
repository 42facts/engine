#ifndef ge_core_resources_resource_hpp
#define ge_core_resources_resource_hpp

#include <string>

namespace ge
{
    namespace core
    {
        namespace resources
        {
            class Resource
            {
            public:
                Resource(std::size_t id);
                ~Resource();

                void loadFromFile(const std::string& filename);

                friend class Manager;

            protected:
                std::size_t getId();
                
                void setName(const std::string& name);
                const std::string& getName();

                void setType(const std::string& type);
                const std::string& getType();
            
            private:
                std::size_t m_id;
                std::string m_name;
                std::string m_type;
            };
        }
    }
}

#endif  // ge_core_resources_resource_hpp