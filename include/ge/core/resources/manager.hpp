#ifndef ge_core_resources_manager_hpp
#define ge_core_resources_manager_hpp

#include <string>
#include <memory>
#include <algorithm>
#include <vector>

#include <ge/core/resources/resource.hpp>

#define HASH_TYPE(x) #x

namespace ge
{
    namespace core
    {
        namespace resources
        {
            class Manager
            {
            public:
                Manager();
                ~Manager();

                template <typename R, typename... Args>
                std::size_t addResource(const std::string& name, const Args&... args)
                {
                    m_resx.push_back(std::make_unique<R>(m_resx.size(), args...));
                    m_resx.back()->setName(name);
                    m_resx.back()->setType(HASH_TYPE(R));
                    return m_resx.size() - 1;
                }

                template <typename R>
                R& getResource(const std::string& name)
                {
                    for (std::size_t i=0; i < m_resx.size(); ++i)
                    {
                        if (m_resx[i]->getName() == name && m_resx[i]->getType() == HASH_TYPE(R))
                            return *dynamic_cast<R*>(m_resx.get());
                    }
                }
            
            private:
                std::vector<std::unique_ptr<Resource>> m_resx;
            };
        }
    }
}

#endif  // ge_core_resources_manager_hpp