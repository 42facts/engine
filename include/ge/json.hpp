#ifndef ge_json_hpp
#define ge_json_hpp

namespace ge
{
    template <typename A>
    std::string atom(A value)
    {
        std::ostringstream os;
        os << value;
        return os.str();
    }

    template <> std::string atom<std::string>(std::string value)
    {
        if (value[0] != '{' && value[0] != '[')
            return "\"" + value + "\"";
        return value;
    }

    template <> std::string atom<const char*>(const char* value)
    {
        if (value[0] != '{' && value[0] != '[')
            return "\"" + std::string(value) + "\"";
        return value;
    }

    template <> std::string atom<std::vector<std::string>>(std::vector<std::string> value)
    {
        std::ostringstream os;
        for (std::size_t i=0; i < value.size(); ++i)
        {
            os << value[i];
            if (i != value.size() - 1)
                os << ", ";
        }
        return os.str();
    }

    template <typename A, typename B>
    std::string json_(const A& key, const B& value)
    {
        std::ostringstream os;
        os << "\"" << key << "\": " << atom(value);
        return os.str();
    }

    template <typename A, typename B, typename... Args>
    std::string json_(const A& key, const B& value, const Args&... args)
    {
        std::ostringstream os;
        os << "\"" << key << "\": " << atom(value) << ", " << json_(args...);
        return os.str();
    }

    template <typename A, typename B>
    std::string json(const A& key, const B& value)
    {
        return "{" + json_(key, value) + "}";
    }

    template <typename A, typename B, typename... Args>
    std::string json(const A& key, const B& value, const Args&... args)
    {
        std::ostringstream os;
        os << "{" << json_(key, value) << ", " << json_(args...) << "}";
        return os.str();
    }

    template <typename A>
    std::string list_(const A& elem)
    {
        return atom(elem);
    }

    template <typename A, typename... Args>
    std::string list_(const A& car, const Args&... cdr)
    {
        std::ostringstream os;
        os << atom(car) << ", " << list_(cdr...);
        return os.str();
    }

    std::string list()
    {
        return "[]";
    }

    template <typename A>
    std::string list(const A& elem)
    {
        return "[" + atom(elem) + "]";
    }

    template <typename A, typename... Args>
    std::string list(const A& car, const Args&... cdr)
    {
        std::ostringstream os;
        os << "[" << atom(car) << ", " << list_(cdr...) << "]";
        return os.str();
    }
}

#endif