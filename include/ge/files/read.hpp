#ifndef ge_files_read_hpp
#define ge_files_read_hpp

#include <string>
#include <fstream>
#include <streambuf>

namespace ge
{
    namespace files
    {
        inline std::string read(const std::string& name)
        {
            std::ifstream f(name.c_str());
            // admitting the file exists
            return std::string(
                (std::istreambuf_iterator<char>(f)),
                std::istreambuf_iterator<char>()
            );
        }
    }
}

#endif  // ge_files_read_hpp