#ifndef ge_files_exists_hpp
#define ge_files_exists_hpp

#include <string>
#include <fstream>

namespace ge
{
    namespace files
    {
        inline bool exists(const std::string& filename)
        {
            std::ifstream file(filename.c_str());
            return file.good();
        }
    }
}

#endif  // ge_files_exists_hpp