#ifndef ge_files_write_hpp
#define ge_files_write_hpp

#include <string>
#include <fstream>

namespace ge
{
    namespace files
    {
        inline void write(const std::string& name, const std::string& content)
        {
            std::ofstream f(name);
            f << content;
            f.close();
        }
    }
}

#endif  // ge_files_write_hpp