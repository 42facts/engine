#ifndef ge_files_cwd_hpp
#define ge_files_cwd_hpp

#include <ge/constants.hpp>

#include <stdio.h>

#if defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
    #include <experimental/filesystem>
#endif

#ifdef GE_SYS_UNIX
    #include <limits.h>
    #include <unistd.h>
#endif

#ifdef GE_SYS_APPLE
    #include <mach-o/dyld.h>
    #include <vector>
#endif

#include <string>

namespace ge
{
    namespace files
    {
        inline std::string cwd()
        {
        #if defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
            return std::experimental::filesystem::current_path().string();
        #elif defined(GE_SYS_UNIX)
            char result[PATH_MAX];
            ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
            return std::string(result, (count > 0) ? count : 0);
        #elif defined(GE_SYS_APPLE)
            unsigned int bufferSize = 512;
            std::vector<char> buffer(bufferSize + 1);
            if(_NSGetExecutablePath(&buffer[0], &bufferSize))
            {
                buffer.resize(bufferSize);
                _NSGetExecutablePath(&buffer[0], &bufferSize);
            }
            return std::string(&buffer[0]);
        #endif
        }
    }
}

#endif  // ge_files_cwd_hpp