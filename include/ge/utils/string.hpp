#ifndef ge_utils_string_hpp
#define ge_utils_string_hpp

#include <string>
#include <sstream>
#include <iostream>
#include <streambuf>
#include <regex>

namespace ge
{
    namespace utils
    {
        template <typename T>
        std::string toString(const T& object)
        {
            std::ostringstream os;
            os << object;
            return os.str();
        }

        template <typename T>
        std::string toString(T&& object)
        {
            std::ostringstream os;
            os << object;
            return os.str();
        }

        inline bool isInteger(const std::string& s)
        {
            return std::regex_match(s, std::regex("^((\\+|-)?[[:digit:]]+)$"));
        }

        inline bool isFloat(const std::string& s)
        {
            return std::regex_match(s, std::regex("^((\\+|-)?[[:digit:]]+)(\\.(([[:digit:]]+)?))$"));
        }
    }
}

#endif  // ge_utils_string_hpp