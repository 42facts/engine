#ifndef ge_constants_hpp
#define ge_constants_hpp

#define GE_DEBUG

namespace ge
{
    namespace version
    {
        constexpr unsigned major = 0;
        constexpr unsigned minor = 0;
        constexpr unsigned patch = 2;
    }

#if defined(_WIN32) && !defined(_WIN64)
    #define GE_SYS_WIN32
#elif defined(_WIN64)
    #define GE_SYS_WIN64
#endif

#if (defined(unix) || defined(__unix) || defined(__unix__)) && !defined(__APPLE__)
    #define GE_SYS_UNIX
#elif defined(__APPLE__)
    #define GE_SYS_APPLE
#endif
}

#endif