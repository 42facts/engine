#ifndef ge_scripting_arkscripting_hpp
#define ge_scripting_arkscripting_hpp

#ifdef abs
    #undef abs
#endif

#include <Ark/Ark.hpp>
#include <string>

namespace ge
{
    namespace scripting
    {
        class ArkScripting
        {
        public:
            ArkScripting(const std::string& libdir);
            ~ArkScripting();

            void registerScript(const std::string& script);
            void loadScript();
            void loadFunction(const std::string& name, Ark::internal::Value::ProcType function);

            Ark::VM& operator()();

        private:
            Ark::State m_state;
            Ark::VM m_vm;
            std::string m_script;
        };
    }
}

#endif  // ge_scripting_arkscripting_hpp