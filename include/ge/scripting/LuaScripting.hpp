#ifndef ge_scripting_luascripting_hpp
#define ge_scripting_luascripting_hpp

#ifdef abs
    #undef abs
#endif

#include <kaguya.hpp>
#include <string>

namespace ge
{
    namespace scripting
    {
        class LuaScripting
        {
        public:
            LuaScripting();
            ~LuaScripting();

            void registerScript(const std::string& script);
            void loadScript();
            kaguya::State& operator()();

        private:
            kaguya::State m_state;
            std::string m_script;
        };
    }
}

#endif  // ge_scripting_luascripting_hpp