# Game Engine 2D

## Dependencies

Language and toolchain
* C++ 17
* CMake >= 3.8

Librairies for the engine
* Lua 5.3.x (scripting)
* ArkScript 3 (scripting)
* Kaguya (library to use Lua more easily)
* SFML 2.5.x (rendering, sound and network)
* termcolor (colored output to terminal)
* jsoncpp (reading json files)
* clipp (reading command line arguments)
* fmt (formatting strings)
* imgui (immediate gui)

Misc
* OpenGL >= 3.3
* Linux / Windows (32/64bits) / Apple

## Building

```bash
> cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Debug
> cmake --build build --config=Debug
```
