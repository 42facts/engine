cmake_minimum_required(VERSION 3.8)

project(LuaLib)

file(GLOB_RECURSE SOURCE_FILES
    ${LuaLib_SOURCE_DIR}/*.c
)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)
add_library(LuaLib STATIC ${SOURCE_FILES})

target_include_directories(LuaLib PUBLIC "${LuaLib_SOURCE_DIR}")