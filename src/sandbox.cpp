#include <iostream>
#include <clipp.hpp>

#include <ge/constants.hpp>
#include <ge/logger.hpp>
#include <ge/config/loader.hpp>
#include <ge/core/engine.hpp>

#include <entt/entt.hpp>
#include <cstdint>

#include <ge/core/scenes/scene.hpp>
#include <ge/core/plugin.hpp>

class MyScene: public ge::core::scenes::ArkScene
{
public:
    MyScene(std::size_t id, int test_arg) :
        ge::core::scenes::ArkScene(id, "assets/scripts/test.ark")
    {
        m_ark.loadFunction("myscene_coucou", [](const std::vector<Ark::Value>& s) {
            for (auto&& u : s)
                std::cout << "ark coucou: " << u << std::endl;
            return Ark::Nil;
        });
    }

    void handleEvent(const sf::Event& event)
    {
        if (event.type == sf::Event::KeyPressed)
            ge::logger.info("(scene) key");
    }

    void update(sf::Time dt)
    {}

    void render(sf::RenderTarget& window)
    {}
private:
};

struct position {
    float x;
    float y;
};

struct velocity {
    float dx;
    float dy;
};

void update(entt::registry<> &registry) {
    auto view = registry.view<position, velocity>();

    for(auto entity: view) {
        // gets only the components that are going to be used ...

        auto &vel = view.get<velocity>(entity);

        vel.dx = 0.;
        vel.dy = 0.;

        // ...
    }
}

void update(std::uint64_t dt, entt::registry<> &registry) {
    registry.view<position, velocity>().each([dt](const auto, auto &pos, auto &vel) {
        // gets all the components of the view at once ...

        pos.x += vel.dx * dt;
        pos.y += vel.dy * dt;

        // ...
    });
}

void test()
{
    /*ge::core::SharedLibrary dll;
    dll.load("./libSMRLT_lib.so.1.0.0");
    // trying to call a method from the shared library
    ge::logger.log(
        "trying to call stuff from a shared library\nreturn value: {}"
        , dll.get<const char*(*)(void)>("testFunc")()
    );

    dll.get<int (*)(void)>("main")();*/

    entt::registry registry = entt::registry<>();
    std::uint64_t dt = 16;

    for(auto i = 0; i < 10; ++i) {
        auto entity = registry.create();
        registry.assign<position>(entity, i * 1.f, i * 1.f);
        if(i % 2 == 0) { registry.assign<velocity>(entity, i * .1f, i * .1f); }
    }

    update(dt, registry);
    update(registry);

    ge::core::Engine engine(640, 480);
    engine.setImGUIDebug(true);

    auto id = engine.addScene<MyScene>(/* test_arg */ 12);
    engine.setCurrentScene(id);

    engine.setEventFunc([&](const sf::Event& event){
        if (event.type == sf::Event::KeyPressed)
        {
            ge::logger.log("event (key)");
        }
    });
    engine.run();
}

int main(int argc, char** argv)
{
    using namespace clipp;

    std::cout << "GameEngine2" << std::endl << std::endl;

    enum class mode {help, version, test, test1, test2};
    mode selected;
    // related to the sub programs
    std::vector<std::string> input_files;
    std::string output_file = "";
    // general flags and stuff
    bool debug = false, experimental = false;
    std::vector<std::string> wrong;

    auto cli = (
        // general options
        option("-h", "--help").set(selected, mode::help).doc("Displays this help message")
        | option("--version").set(selected, mode::version).doc("Displays the version and exits")
        | command("test").set(selected, mode::test).doc("Testing mode, to test stuff")
        // sub-programs
        | (
            // example
            (
                (command("test1").set(selected, mode::test1).doc("Launch the test 1 program")
                  )
                | (command("test2").set(selected, mode::test2).doc("Launch the test 2 program")
                  )
                , values("files", input_files)
                , required("-o", "--out") & value("output", output_file)
              )
            // options attached to all those sub-programs
            , option("-d", "--debug").set(debug).doc("Enable debug mode")
            , option("-E", "--experimental").set(experimental).doc("Enable experimental features")
          )
        , any_other(wrong)
    );

    auto fmt = doc_formatting{}
        .start_column(8)           // column where usage lines and documentation starts
        .doc_column(36)            // parameter docstring start col
        .indent_size(2)            // indent of documentation lines for children of a documented group
        .split_alternatives(true)  // split usage into several lines for large alternatives
    ;

    if (parse(argc, argv, cli) && wrong.empty())
    {
        switch (selected)
        {
        default:
        case mode::help:
            std::cerr << make_man_page(cli, argv[0], fmt).append_section("LICENSE", "        Mozilla Public License 2.0")
                      << std::endl;
            return 0;

        case mode::version:
            std::cout << "Version " << ge::version::major << "." << ge::version::minor << "." << ge::version::patch << std::endl;
            break;
        
        case mode::test:
            test();
            break;
        
        case mode::test1:
            std::cout << "test1" << std::endl;
            break;
        
        case mode::test2:
            std::cout << "test2" << std::endl;
            break;
        }
    }
    else
    {
        for (const auto& arg : wrong)
            std::cerr << "'" << arg << "'" << " ins't a valid argument" << std::endl;
            
        std::cerr << "Usage:"   << std::endl << usage_lines(cli, argv[0], fmt) << std::endl
                  << "Options:" << std::endl << documentation(cli, fmt) << std::endl
                  << "LICENSE"  << std::endl << "        Mozilla Public License 2.0" << std::endl;
    }

    return 0;
}