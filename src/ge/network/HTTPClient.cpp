#include <ge/network/HTTPClient.hpp>

namespace ge
{
    namespace network
    {
        HTTPClient::HTTPClient(const char* host, int port) :
            m_mainRoute("")
            , m_client(host, port)
        {}

        HTTPClient::~HTTPClient()
        {}

        void HTTPClient::setMainRoute(const std::string& mainRoute)
        {
            m_mainRoute = mainRoute;
        }

        void HTTPClient::post(const std::string& route, const std::string& msg, HTTPClient::Callback_t callback)
        {
            std::thread t([this, route, msg] (HTTPClient::Callback_t&& callback) -> void {
                m_mutex.lock();

                if (!m_headers.empty())
                {
                    auto res = m_client.Post((m_mainRoute + route).c_str(), m_headers, msg, "application/json");
                    callback(res);
                }
                else
                {
                    auto res = m_client.Post((m_mainRoute + route).c_str(), msg, "application/json");
                    callback(res);
                }

                m_mutex.unlock();
            }, std::move(callback));

            t.detach();
        }

        void HTTPClient::get(const std::string& route, HTTPClient::Callback_t callback)
        {
            std::thread t([this, route] (HTTPClient::Callback_t&& callback) -> void {
                m_mutex.lock();

                if (!m_headers.empty())
                {
                    auto res = m_client.Get((m_mainRoute + route).c_str(), m_headers);
                    callback(res);
                }
                else
                {
                    auto res = m_client.Get((m_mainRoute + route).c_str());
                    callback(res);
                }

                m_mutex.unlock();
            }, std::move(callback));

            t.detach();
        }

        void HTTPClient::waitPost(const std::string& route, const std::string& msg, HTTPClient::Callback_t callback)
        {
            m_mutex.lock();

            if (!m_headers.empty())
            {
                auto res = m_client.Post((m_mainRoute + route).c_str(), m_headers, msg, "application/json");
                callback(res);
            }
            else
            {
                auto res = m_client.Post((m_mainRoute + route).c_str(), msg, "application/json");
                callback(res);
            }

            m_mutex.unlock();
        }

        void HTTPClient::waitGet(const std::string& route, HTTPClient::Callback_t callback)
        {
            m_mutex.lock();

            if (!m_headers.empty())
            {
                auto res = m_client.Get((m_mainRoute + route).c_str(), m_headers);
                callback(res);
            }
            else
            {
                auto res = m_client.Get((m_mainRoute + route).c_str());
                callback(res);
            }

            m_mutex.unlock();
        }

        void HTTPClient::setHeader(const std::string& name, const std::string& value)
        {
            m_headers.emplace(name, value);
        }
    }
}