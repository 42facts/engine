#include <ge/core/scenes/scene.hpp>

#include <ge/logger.hpp>

namespace ge
{
    namespace core
    {
        namespace scenes
        {
            Scene::Scene(std::size_t id) :
                m_id(id),
                m_state(State::Stop)
            {}

            std::size_t Scene::getId()
            {
                return m_id;
            }

            State Scene::getState()
            {
                return m_state;
            }

            void Scene::setState(State state)
            {
                m_state = state;
            }

            void Scene::_init()
            {}

            void Scene::_handleEvent(const sf::Event& event)
            {
                handleEvent(event);
            }
            
            void Scene::_update(sf::Time dt)
            {
                update(dt);
            }
            
            void Scene::_render(sf::RenderTarget& window)
            {
                render(window);
            }

            // --------------------------------------------

            LuaScene::LuaScene(std::size_t id, const std::string& script) :
                Scene(id)
            {
                m_lua.registerScript(script);
            }

            LuaScene::~LuaScene()
            {}

            void LuaScene::setState(State state)
            {
                m_state = state;

                if (m_state == State::Running)
                    m_lua()["onStateChange"]("running");
                else if (m_state == State::Idle)
                    m_lua()["onStateChange"]("idle");
                else if (m_state == State::Stop)
                    m_lua()["onStateChange"]("stop");
            }

            void LuaScene::_init()
            {
                m_lua.loadScript();
            }

            void LuaScene::_handleEvent(const sf::Event& event)
            {
                // TODO
                // m_lua()["onEvent"](event); kaguya::NewTable()
                handleEvent(event);
            }
            
            void LuaScene::_update(sf::Time dt)
            {
                m_lua()["onUpdate"](dt.asSeconds());
                update(dt);
            }
            
            void LuaScene::_render(sf::RenderTarget& window)
            {
                // TODO
                // m_lua()["onRender"](window); kaguya::NewTable()
                render(window);
            }

            // --------------------------------------------

            ArkScene::ArkScene(std::size_t id, const std::string& script, const std::string& libdir) :
                Scene(id)
                , m_ark(libdir)
            {
                m_ark.registerScript(script);
            }

            ArkScene::~ArkScene()
            {}

            void ArkScene::setState(State state)
            {
                m_state = state;

                if (m_state == State::Running)
                    m_ark().call("onStateChange", std::string("running"));
                else if (m_state == State::Idle)
                    m_ark().call("onStateChange", std::string("idle"));
                else if (m_state == State::Stop)
                    m_ark().call("onStateChange", std::string("stop"));
            }

            void ArkScene::_init()
            {
                m_ark.loadScript();
            }

            void ArkScene::_handleEvent(const sf::Event& event)
            {
                // TODO
                // m_ark().call("onEvent", event)
                handleEvent(event);
            }
            
            void ArkScene::_update(sf::Time dt)
            {
                m_ark().call("onUpdate", dt.asSeconds());
                update(dt);
            }
            
            void ArkScene::_render(sf::RenderTarget& window)
            {
                // TODO
                // m_ark().call("onRender", window);
                render(window);
            }
        }
    }
}