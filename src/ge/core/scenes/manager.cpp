#include <ge/core/scenes/manager.hpp>

#include <ge/logger.hpp>
#include <ge/utils/string.hpp>
#include <algorithm>

namespace ge
{
    namespace core
    {
        namespace scenes
        {
            Manager::Manager() :
                m_current(0),
                m_active(false)
            {}

            Manager::~Manager()
            {
                for (std::size_t i=0; i < m_scenes.size(); ++i)
                    m_scenes[i]->setState(State::Stop);
            }

            void Manager::removeScene(std::size_t id)
            {
                if (id == m_current)
                    m_active = false;

                auto it = std::find_if(m_scenes.begin(), m_scenes.end(), [id](auto& scene) -> bool {
                    return scene->getId() == id;
                });

                if (it != m_scenes.end())
                    m_scenes.erase(it);
            }

            Scene* Manager::getCurrentScene()
            {
                if (m_active)
                    return m_scenes[m_current].get();
                
                ge::logger.warn("(ge::core::scenes::Manager::getCurrentScene) no current scene");
                return nullptr;
            }

            void Manager::setCurrentScene(std::size_t id)
            {
                if (id < m_scenes.size())
                {
                    m_active = true;
                    m_scenes[m_current]->setState(State::Idle);
                    m_current = id;
                    m_scenes[m_current]->setState(State::Running);
                }
                else
                    ge::logger.warn("(ge::core::scenes::Manager::setCurrentScene) id is too big: " + ge::utils::toString(id));
            }

            void Manager::handleEvent(const sf::Event& event)
            {
                if (m_active)
                    m_scenes[m_current]->_handleEvent(event);
            }

            void Manager::update(sf::Time dt)
            {
                if (m_active)
                    m_scenes[m_current]->_update(dt);
                
                for (std::size_t i=0; i < m_scenes.size(); ++i)
                {
                    if (m_scenes[i]->getState() == State::Idle)
                        m_scenes[i]->_update(dt);
                }
            }

            void Manager::render(sf::RenderTarget& window)
            {
                for (std::size_t i=0; i < m_scenes.size(); ++i)
                {
                    if (m_scenes[i]->getState() == State::Idle)
                        m_scenes[i]->_render(window);
                }

                if (m_active)
                    m_scenes[m_current]->_render(window);
            }
        }
    }
}