#include <ge/core/plugin.hpp>

#include <sstream>
#include <iomanip>
#include <ge/files/exists.hpp>
#include <iostream>

namespace ge
{
    namespace core
    {
        SharedLibrary::SharedLibrary() :
            m_hInstance(NULL)
            , m_path("")
            , m_loaded(false)
        {}

        SharedLibrary::SharedLibrary(const std::string& path) :
            m_hInstance(NULL)
            , m_path(path)
            , m_loaded(false)
        {
            load(m_path);
        }

        SharedLibrary::~SharedLibrary()
        {
            unload();
        }

        void SharedLibrary::load(const std::string& path)
        {
            if (m_loaded)
                unload();
            
            m_path = path;
            if (!ge::files::exists(m_path))
                throw std::runtime_error("Couldn't find the library " + path);
            
#if defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
            if (NULL == (m_hInstance = LoadLibrary(m_path.c_str())))
            {
                throw std::system_error(
                    std::error_code(::GetLastError(), std::system_category())
                    , "Couldn't load the library at " + path
                );
            }
#elif defined(GE_SYS_UNIX) || defined(GE_SYS_APPLE)
            if (NULL == (m_hInstance = dlopen(m_path.c_str(), RTLD_NOW | RTLD_GLOBAL)))
            {
                throw std::system_error(
                    std::error_code(errno, std::system_category())
                    , "Couldn't load the library at " + path + ", " + std::string(dlerror())
                );
            }
#endif
            m_loaded = true;
        }

        void SharedLibrary::unload()
        {
            if (m_loaded)
            {
#if defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
                FreeLibrary(m_hInstance);
#elif defined(GE_SYS_UNIX) || defined(GE_SYS_APPLE)
                dlclose(m_hInstance);
#endif
            }
        }

    }
} 