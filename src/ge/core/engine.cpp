#include <ge/core/engine.hpp>

namespace ge
{
    namespace core
    {
        Engine::Engine(unsigned width, unsigned height) :
            m_window(sf::VideoMode(width, height), ""),
            m_eventFunc(nullptr),
            m_updateFunc(nullptr),
            m_renderFunc(nullptr),
            m_imgui_debug(false)
        {
            setVSync(true);

            ImGui::SFML::Init(m_window);
            m_window.resetGLStates();
        }

        Engine::~Engine()
        {
            // must be called after m_window.close()
            ImGui::SFML::Shutdown();
        }

        void Engine::run()
        {
            while (m_window.isOpen())
            {
                auto dt = m_clock.restart();

                handleEvent();
                update(dt);
                render();
            }
        }
        
        void Engine::handleEvent()
        {
            sf::Event event;
            while (m_window.pollEvent(event))
            {
                // basic event handling before anything else
                if (event.type == sf::Event::Closed)
                    m_window.close();

                ImGui::SFML::ProcessEvent(event);
                
                if (m_eventFunc != nullptr)
                    m_eventFunc(event);
                
                m_sceneManager.handleEvent(event);
            }
        }
        
        void Engine::update(sf::Time dt)
        {
            if (m_updateFunc != nullptr)
                m_updateFunc(dt);
            ImGui::SFML::Update(m_window, dt);
            m_sceneManager.update(dt);
        }
        
        void Engine::render()
        {
            if (m_imgui_debug)
            {
                ImGui::Begin("Sample window"); // begin window

                                            // Background color edit
                if (ImGui::ColorEdit3("Background color", m_imgui.color)) {
                    // this code gets called if color value changes, so
                    // the background color is upgraded automatically!
                    m_imgui.bgColor.r = static_cast<sf::Uint8>(m_imgui.color[0] * 255.f);
                    m_imgui.bgColor.g = static_cast<sf::Uint8>(m_imgui.color[1] * 255.f);
                    m_imgui.bgColor.b = static_cast<sf::Uint8>(m_imgui.color[2] * 255.f);
                }

                // Window title text edit
                ImGui::InputText("Window title", m_imgui.windowTitle, 255);

                if (ImGui::Button("Update window title")) {
                    // this code gets if user clicks on the button
                    // yes, you could have written if(ImGui::InputText(...))
                    // but I do this to show how buttons work :)
                    m_window.setTitle(m_imgui.windowTitle);
                }
                ImGui::End(); // end window

                m_window.clear(m_imgui.bgColor); // fill background with color
                ImGui::SFML::Render(m_window);
            }
            else
            {
                m_window.clear();
            }

            m_sceneManager.render(m_window);

            if (m_renderFunc != nullptr)
                m_renderFunc(m_window);

            m_window.display();
        }

        void Engine::setTitle(const std::string& name)
        {
            m_window.setTitle(name);
        }

        void Engine::setVSync(bool value)
        {
            m_window.setVerticalSyncEnabled(value);
        }

        void Engine::setImGUIDebug(bool value)
        {
            m_imgui_debug = value;
        }

        void Engine::setFPSLimit(int value)
        {
            m_window.setFramerateLimit(value);
        }

        void Engine::setEventFunc(EventFunc event)
        {
            m_eventFunc = event;
        }

        void Engine::setUpdateFunc(UpdateFunc update)
        {
            m_updateFunc = update;
        }

        void Engine::setRenderFunc(RenderFunc render)
        {
            m_renderFunc = render;
        }

        void Engine::removeScene(std::size_t id)
        {
            m_sceneManager.removeScene(id);
        }

        void Engine::setCurrentScene(std::size_t id)
        {
            m_sceneManager.setCurrentScene(id);
        }
    }
}