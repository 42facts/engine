#include <ge/scripting/LuaScripting.hpp>
#include <ge/files/exists.hpp>
#include <ge/files/cwd.hpp>
#include <ge/logger.hpp>

namespace ge
{
    namespace scripting
    {
        LuaScripting::LuaScripting()
        {
            m_state["getProjectRoot"] = &ge::files::cwd;
        }

        LuaScripting::~LuaScripting()
        {}

        void LuaScripting::registerScript(const std::string& script)
        {
            if (ge::files::exists(script))
                m_script = script;
            else
                ge::logger.error("(ge::scripting::LuaScripting::register) Couldn't open script '" + script + "'");
        }

        void LuaScripting::loadScript()
        {
            if (!m_state.dofile(m_script))
                ge::logger.error("(state.dofile) Couldn't open or run given file");
        }

        kaguya::State& LuaScripting::operator()()
        {
            return m_state;
        }
    }
}