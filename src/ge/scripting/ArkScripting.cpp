#include <ge/scripting/ArkScripting.hpp>
#include <ge/files/exists.hpp>
#include <ge/files/cwd.hpp>
#include <ge/logger.hpp>

namespace ge
{
    namespace scripting
    {
        ArkScripting::ArkScripting(const std::string& libdir) :
            m_state(libdir, Ark::FeaturePersist), m_vm(&m_state), m_script("")
        {
            m_state.loadFunction("getProjectRoot", [](const std::vector<Ark::Value>& args) -> Ark::Value {
                return Ark::Value(ge::files::cwd());
            });
        }

        ArkScripting::~ArkScripting()
        {}

        void ArkScripting::registerScript(const std::string& script)
        {
            if (ge::files::exists(script))
                m_script = script;
            else
                ge::logger.error("(ge::scripting::ArkScripting::register) Couldn't open script '" + script + "'");
        }

        void ArkScripting::loadScript()
        {
            // compile if need and run
            m_state.doFile(m_script);
            m_vm.run();
        }

        void ArkScripting::loadFunction(const std::string& name, Ark::internal::Value::ProcType function)
        {
            m_state.loadFunction(name, std::move(function));
        }

        Ark::VM& ArkScripting::operator()()
        {
            return m_vm;
        }
    }
}