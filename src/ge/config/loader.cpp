#include <ge/config/loader.hpp>
#include <ge/logger.hpp>
#include <fstream>
#include <sstream>

namespace ge
{
    namespace config
    {
        Loader::Loader(const std::string& file) :
            m_filename(file)
        {
            if (!ge::files::exists(m_filename))
            {
                ge::logger.warn("Couldn't load configuration file: " + m_filename + "\nUsing an empty configuration");
                std::istringstream stream("{}");
                stream >> m_content;
            }
            else
            {
                std::ifstream f(m_filename.c_str());
                f >> m_content;
                f.close();
            }
        }

        Loader::~Loader()
        {
            std::ofstream f(m_filename.c_str());
            f << m_content;
            f.close();
        }

        void Loader::reload()
        {
            std::ifstream f(m_filename.c_str());
            f >> m_content;
            f.close();
        }
    }
}